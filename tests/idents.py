#!/usr/bin/env python

import pyIDL
import os

def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	if tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	if tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())

	# This is the real test - once we've got to the ident, we
	# assert that the referred node is the structure
	if tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		ident = tree.get_typespec()
		assert ident.get_name(1)=="foo","Ident is a foo"
		
		structure = ident.get_referred_node()
		assert structure.get_name(1)=="foo","Structure is foo" 
		assert structure.get_type() == pyIDL.IDLN_TYPE_STRUCT, "foo is a structure"
		


tree = pyIDL.parse_file(os.environ["srcdir"]+"/idents.idl")
process_node(tree)
