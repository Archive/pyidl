%module pyIDLBackend
%{
#include <glib.h>
#include <libIDL/IDL.h>


extern IDL_tree parse_file(const char* filename) {
  IDL_ns namespace;
  IDL_tree tree;
  int res; 
  GString *cpp_args;

  cpp_args = g_string_new("-D__ORBIT_IDL__ ");

  res = IDL_parse_filename(filename, cpp_args->str , NULL, &tree, &namespace,
			 0,0);
  if(IDL_SUCCESS != res) {
	if (res < 0)
	  g_print("Parse of %s failed: %s\n", filename, g_strerror(errno));
	else
	  g_print("Parse of %s failed\n", filename);

	g_print("I should also tell you why it failed, but I haven't got round to doing that yet.\nIn the meantime as a workaround, run orbit-idl on your idl file and see what error it gives :-)\n");

	exit(1);
  }
  return tree;
}

const char* 
ns_ident_to_qstring(IDL_tree tree, char* seperator, int ident){
  char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(tree),
									   seperator,
									   ident);
  return name; 
} 

const char* ident_str(IDL_tree tree) {
  return IDL_IDENT(tree).str;
}

IDL_tree_type node_type(IDL_tree tree){
  return IDL_NODE_TYPE(tree);
}

IDL_tree node_up(IDL_tree tree){
  return IDL_NODE_UP(tree);
}

IDL_tree list_next(IDL_tree list){
  IDL_tree next = IDL_LIST(list).next;
  return next;
}

IDL_tree list_data(IDL_tree list){
  return IDL_LIST(list).data;
}


IDL_tree interface_ident(IDL_tree tree){
   return IDL_INTERFACE(tree).ident;
}


IDL_tree interface_inheritance_spec(IDL_tree tree){
   return IDL_INTERFACE(tree).inheritance_spec;
}

IDL_tree interface_body(IDL_tree tree){
   return IDL_INTERFACE(tree).body;
}


IDL_tree module_ident(IDL_tree tree){
   return IDL_MODULE(tree).ident;
}

IDL_tree forwardDcl_ident(IDL_tree tree){
   return IDL_FORWARD_DCL(tree).ident;
}

IDL_tree module_definition_list(IDL_tree tree){
   return IDL_MODULE(tree).definition_list;
}

IDL_tree exceptDcl_ident(IDL_tree tree){
   return IDL_EXCEPT_DCL(tree).ident;
}

IDL_tree exceptDcl_members(IDL_tree tree){
   return IDL_EXCEPT_DCL(tree).members;
}

IDL_tree member_typespec(IDL_tree tree) {
  return IDL_MEMBER(tree).type_spec;
}

IDL_tree member_dcls(IDL_tree tree) {
  return IDL_MEMBER(tree).dcls;
}

IDL_tree typeDcl_dcls(IDL_tree tree){
	return IDL_TYPE_DCL(tree).dcls;
}

IDL_tree typeDcl_type_spec(IDL_tree tree){
  	return IDL_TYPE_DCL(tree).type_spec;
}

IDL_tree sequence_simple_type_spec(IDL_tree tree){
	return IDL_TYPE_SEQUENCE(tree).simple_type_spec;   
}

IDL_tree sequence_positive_int_const(IDL_tree tree){
	return IDL_TYPE_SEQUENCE(tree).positive_int_const;
}

IDL_tree opDcl_ident(IDL_tree tree){
   return IDL_OP_DCL(tree).ident;
}

IDL_tree opDcl_op_type_spec(IDL_tree tree) {
  return IDL_OP_DCL(tree).op_type_spec;
}

IDL_tree opDcl_parameter_dcls(IDL_tree tree) {
  return IDL_OP_DCL(tree).parameter_dcls;
}

IDL_tree opDcl_raises_expr(IDL_tree tree) {
  return IDL_OP_DCL(tree).raises_expr;
}

IDL_tree paramDcl_simple_declarator(IDL_tree tree) {
  return IDL_PARAM_DCL(tree).simple_declarator;
}

IDL_tree paramDcl_param_type_spec(IDL_tree tree) {
  return IDL_PARAM_DCL(tree).param_type_spec;
}

int paramDcl_attr(IDL_tree tree) {
  return IDL_PARAM_DCL(tree).attr;
}

IDL_tree struct_ident(IDL_tree tree){
   return IDL_TYPE_STRUCT(tree).ident;
}

IDL_tree struct_member_list(IDL_tree tree){
  return IDL_TYPE_STRUCT(tree).member_list;
}

IDL_tree const_ident(IDL_tree tree){
  return IDL_CONST_DCL(tree).ident;	
}

IDL_tree const_type(IDL_tree tree){
  return IDL_CONST_DCL(tree).const_type;	
}

IDL_tree const_exp(IDL_tree tree){
  return IDL_CONST_DCL(tree).const_exp;	
}

IDL_tree enum_ident(IDL_tree tree){
   return IDL_TYPE_ENUM(tree).ident;
}

IDL_tree enum_enumerator_list(IDL_tree tree){
   return IDL_TYPE_ENUM(tree).enumerator_list;
}


const char* ident_repoid(IDL_tree tree){
  return IDL_IDENT_REPO_ID(tree);
}

enum IDL_integer_type type_integer_f_type(IDL_tree tree) {
  return IDL_TYPE_INTEGER(tree).f_type;
}

int type_integer_f_signed(IDL_tree tree) {
  return IDL_TYPE_INTEGER(tree).f_signed;
}

enum IDL_float_type type_float_f_type(IDL_tree tree) {
  return IDL_TYPE_FLOAT(tree).f_type;
}

int integer_value(IDL_tree tree) {
  return IDL_INTEGER(tree).value;
}

const char* string_value(IDL_tree tree) {
  return IDL_STRING(tree).value;
}

unsigned boolean_value(IDL_tree tree) {
  return IDL_BOOLEAN(tree).value;
}

int print_something() {
   g_print("something");
}

int is_null(IDL_tree tree) {
  if(tree == 0) 
	return 1;   // TRUE
  else 
	return 0;   // FALSE
}

%}

enum IDL_param_attr {
	IDL_PARAM_IN,
	IDL_PARAM_OUT,
	IDL_PARAM_INOUT
};


enum IDL_integer_type {
	IDL_INTEGER_TYPE_SHORT,
	IDL_INTEGER_TYPE_LONG,
	IDL_INTEGER_TYPE_LONGLONG
};

enum IDL_float_type {
	IDL_FLOAT_TYPE_FLOAT,
	IDL_FLOAT_TYPE_DOUBLE,
	IDL_FLOAT_TYPE_LONGDOUBLE
};

typedef enum {
	IDLN_NONE,
	IDLN_ANY,

	IDLN_LIST,
	IDLN_GENTREE,
	IDLN_INTEGER,
	IDLN_STRING,
	IDLN_WIDE_STRING,
	IDLN_CHAR,
	IDLN_WIDE_CHAR,
	IDLN_FIXED,
	IDLN_FLOAT,
	IDLN_BOOLEAN,
	IDLN_IDENT,
	IDLN_TYPE_DCL,
	IDLN_CONST_DCL,
	IDLN_EXCEPT_DCL,
	IDLN_ATTR_DCL,
	IDLN_OP_DCL,
	IDLN_PARAM_DCL,
	IDLN_FORWARD_DCL,
	IDLN_TYPE_INTEGER,
	IDLN_TYPE_FLOAT,
	IDLN_TYPE_FIXED,
	IDLN_TYPE_CHAR,
	IDLN_TYPE_WIDE_CHAR,
	IDLN_TYPE_STRING,
	IDLN_TYPE_WIDE_STRING,
	IDLN_TYPE_BOOLEAN,
	IDLN_TYPE_OCTET,
	IDLN_TYPE_ANY,
	IDLN_TYPE_OBJECT,
	IDLN_TYPE_TYPECODE,
	IDLN_TYPE_ENUM,
	IDLN_TYPE_SEQUENCE,
	IDLN_TYPE_ARRAY,
	IDLN_TYPE_STRUCT,
	IDLN_TYPE_UNION,
	IDLN_MEMBER,
	IDLN_NATIVE,
	IDLN_CASE_STMT,
	IDLN_INTERFACE,
	IDLN_MODULE,
	IDLN_BINOP,
	IDLN_UNARYOP,
	IDLN_CODEFRAG,

	IDLN_LAST
} IDL_tree_type;


IDL_tree parse_file(const char* filename);
int print_something();

IDL_tree_type node_type(IDL_tree tree);
IDL_tree node_up(IDL_tree tree);

const char* ns_ident_to_qstring(IDL_tree tree, char* seperator, int ident);
const char* ident_str(IDL_tree tree);
const char* ident_repoid(IDL_tree tree);
IDL_tree list_next(IDL_tree list);
IDL_tree list_data(IDL_tree list);
IDL_tree interface_ident(IDL_tree tree);
IDL_tree interface_inheritance_spec(IDL_tree tree);
IDL_tree interface_body(IDL_tree tree);
IDL_tree module_ident(IDL_tree tree);
IDL_tree module_definition_list(IDL_tree tree);
IDL_tree exceptDcl_ident(IDL_tree tree);
IDL_tree exceptDcl_members(IDL_tree tree);
IDL_tree opDcl_ident(IDL_tree tree);
IDL_tree opDcl_op_type_spec(IDL_tree tree);
IDL_tree opDcl_parameter_dcls(IDL_tree tree);
IDL_tree opDcl_raises_expr(IDL_tree tree);
IDL_tree paramDcl_simple_declarator(IDL_tree tree);
IDL_tree paramDcl_param_type_spec(IDL_tree tree);
int paramDcl_attr(IDL_tree tree);

IDL_tree const_ident(IDL_tree tree);
IDL_tree const_type(IDL_tree tree);
IDL_tree const_exp(IDL_tree tree);

IDL_tree forwardDcl_ident(IDL_tree tree);

IDL_tree typeDcl_dcls(IDL_tree tree);
IDL_tree typeDcl_type_spec(IDL_tree tree);

IDL_tree sequence_simple_type_spec(IDL_tree tree);
IDL_tree sequence_positive_int_const(IDL_tree tree);

IDL_tree enum_ident(IDL_tree tree);
IDL_tree enum_enumerator_list(IDL_tree tree);

IDL_tree struct_ident(IDL_tree tree);
IDL_tree struct_member_list(IDL_tree tree);
IDL_tree member_typespec(IDL_tree tree);
IDL_tree member_dcls(IDL_tree tree);
int integer_value(IDL_tree tree);
const char* string_value(IDL_tree tree);
unsigned boolean_value(IDL_tree tree);
enum IDL_integer_type type_integer_f_type(IDL_tree tree);
int type_integer_f_signed(IDL_tree tree);
enum IDL_float_type type_float_f_type(IDL_tree tree);
int is_null(IDL_tree tree);
