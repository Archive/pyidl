import pyIDLBackend

# this is a hack to obtain the typedef declarations from
#   the idents 
typedef_hash = {}

def set_separator(separator):
	IDLIdent.separator = separator

def parse_file(filename):
	rawtree = pyIDLBackend.parse_file(filename)
	return IDLList(rawtree)

class IDLTree:
	"IDLTree"
	def __init__(self,rawtree):
		self.rawtree = rawtree
	def get_type(self):
		return pyIDLBackend.node_type(self.rawtree)
		
		
class IDLIdent(IDLTree):
	"IDLIdent"
	separator="::"

	def get_name(self,indent):
		return pyIDLBackend.ns_ident_to_qstring(
			self.get_raw_ident() ,IDLIdent.separator,indent
			)

	def get_repoid(self):
		return pyIDLBackend.ident_repoid(self.get_raw_ident())
	
	"""This method is overridden in composite ident objects"""
	def get_raw_ident(self):
		return self.rawtree

	""" Returns the node that this identifier identifies"""
	def get_referred_node(self):
		repoid = self.get_repoid()
		if typedef_hash.has_key(repoid):
			return typedef_hash[repoid]
		return tree_factory(pyIDLBackend.node_up(self.get_raw_ident()))
	
	
class IDLList(IDLTree):
	"IDLList"

	def __getitem__(self, index):
		# recurse through the list structure and return the
		# appropriate tree  
		list = self

		while index > 0 and list != None:
			list = list.next()
			index = index-1	
		if list != None:
			return list.get_tree()
		else:
			raise IndexError
		
	def get_tree(self):
		rawtree = pyIDLBackend.list_data(self.rawtree)
		return tree_factory(rawtree)

	def next(self):
		rawlist = pyIDLBackend.list_next(self.rawtree)
		if pyIDLBackend.is_null(rawlist) == 1:
			return None
		else:
			return IDLList(rawlist)

	def __len__(self):
		len=0
		for i in self:
			len=len+1
		return len

"""
A zero length list - saves the code-producer from having
to test for null lists
"""
class NULL_IDLList:
	"Null IDLList"
	def __getitem__(self, index):
		raise IndexError

	def __len__(self):
		return 0
		
class IDLInterface(IDLIdent):
	"IDLInterface"
	def get_raw_ident(self):
		return pyIDLBackend.interface_ident(self.rawtree)
	
	def get_inheritance_spec(self):
		spec = pyIDLBackend.interface_inheritance_spec(self.rawtree)
		if pyIDLBackend.is_null(spec) == 1:
			return NULL_IDLList()
		else:
			return IDLList(spec)
		
	def get_contents(self):
		return IDLList(pyIDLBackend.interface_body(self.rawtree))

class IDLModule(IDLIdent):
	"IDLModule"
		
	def get_raw_ident(self):
		return pyIDLBackend.module_ident(self.rawtree)
	def get_contents(self):
		return IDLList(pyIDLBackend.module_definition_list(self.rawtree))

class IDLForwardDcl(IDLIdent):
	def get_raw_ident(self):
		return pyIDLBackend.forwardDcl_ident(self.rawtree)

class IDLExceptDcl(IDLIdent):
	"IDLExceptDcl"

	def get_contents(self):
		rawlist = pyIDLBackend.exceptDcl_members(self.rawtree)
		if pyIDLBackend.is_null(rawlist) == 1:
			return NULL_IDLList()
		else:
			return IDLList(rawlist)
	def get_raw_ident(self):
		return pyIDLBackend.exceptDcl_ident(self.rawtree)


class IDLMember(IDLTree):
	"IDLMember"
	def get_typespec(self):
		return tree_factory(pyIDLBackend.member_typespec(self.rawtree))
	def get_dcls(self):
		return IDLList(pyIDLBackend.member_dcls(self.rawtree))

class IDLOpDcl(IDLIdent):
	"IDLOpDcl"	

	def get_raw_ident(self):
		return pyIDLBackend.opDcl_ident(self.rawtree)
	def get_return_typespec(self):
		rawtree = pyIDLBackend.opDcl_op_type_spec(self.rawtree)
		if pyIDLBackend.is_null(rawtree) == 1:
			return IDLTypeVoid()
		else:
			return tree_factory(rawtree)
	def get_arguments(self):
		rawlist = pyIDLBackend.opDcl_parameter_dcls(self.rawtree)
		if pyIDLBackend.is_null(rawlist) == 1:
			return NULL_IDLList()
		else:
			return IDLList(rawlist)
	def get_exceptions(self):
		rawlist = pyIDLBackend.opDcl_raises_expr(self.rawtree)
		if pyIDLBackend.is_null(rawlist) == 1:
			return NULL_IDLList()
		else:
			return IDLList(rawlist)

class IDLParamDcl(IDLIdent):
	"IDLParamDcl"	
	IN = pyIDLBackend.IDL_PARAM_IN
	OUT = pyIDLBackend.IDL_PARAM_OUT
	INOUT = pyIDLBackend.IDL_PARAM_INOUT

	def get_raw_ident(self):
		return pyIDLBackend.paramDcl_simple_declarator(self.rawtree)
	"""
	get name is overridden because params don't have a scoped name
	and so don't need an indent parameter
	"""
	def get_name(self):
		return pyIDLBackend.ident_str(self.get_raw_ident())
	def get_typespec(self):
		return tree_factory( \
			pyIDLBackend.paramDcl_param_type_spec(self.rawtree))
	def get_attr(self):
		return pyIDLBackend.paramDcl_attr(self.rawtree)

class IDLConstDcl(IDLIdent):
	def get_typespec(self):
		return tree_factory( \
			pyIDLBackend.const_type(self.rawtree))

	def get_raw_ident(self):
		return pyIDLBackend.const_ident(self.rawtree)

	
	def get_exp(self):
		return tree_factory( \
			pyIDLBackend.const_exp(self.rawtree))

class IDLStruct(IDLIdent):
	"IDLStruct"	

	def get_raw_ident(self):
		return pyIDLBackend.struct_ident(self.rawtree)
	def get_contents(self):
		return IDLList(pyIDLBackend.struct_member_list(self.rawtree))
	

class IDLTypeString(IDLTree):
	"IDLTypeString"	
		
class IDLTypeBoolean(IDLTree):
	"IDLTypeBoolean"

class IDLTypeInteger(IDLTree):
	"IDLTypeInteger"
	SHORT = pyIDLBackend.IDL_INTEGER_TYPE_SHORT
	LONG = pyIDLBackend.IDL_INTEGER_TYPE_LONG
	LONGLONG = pyIDLBackend.IDL_INTEGER_TYPE_LONGLONG
	
	def get_width(self):
		return pyIDLBackend.type_integer_f_type(self.rawtree)

	def is_signed(self):
		return pyIDLBackend.type_integer_f_signed(self.rawtree)
	
class IDLTypeFloat(IDLTree):
	"IDLTypeFloat"
	FLOAT = pyIDLBackend.IDL_FLOAT_TYPE_FLOAT
	DOUBLE = pyIDLBackend.IDL_FLOAT_TYPE_DOUBLE
	LONGDOUBLE = pyIDLBackend.IDL_FLOAT_TYPE_LONGDOUBLE

	def get_floattype(self):
		return pyIDLBackend.type_float_f_type(self.rawtree)
		
class IDLTypeVoid:
	"IDLTypeVoid"
		
	def get_type(self):
		return IDLN_VOID

class IDLInteger(IDLTree):
	"IDLInteger"
	def get_value(self):
		return pyIDLBackend.integer_value(self.rawtree)

class IDLString(IDLTree):
	"IDLString"
	def get_value(self):
		return pyIDLBackend.string_value(self.rawtree)

class IDLBoolean(IDLTree):
	"IDLBoolean"
	def get_value(self):
		return pyIDLBackend.boolean_value(self.rawtree)

class IDLTypeDcl(IDLTree):
	"IDLTypeDcl"
	def __init__(self,rawtree):
		IDLTree.__init__(self,rawtree)
		# populate the typedef hash
		for ident in self.get_dcls():
			typedef_hash[ident.get_repoid()]=self
		
	def get_typespec(self):
		return tree_factory(pyIDLBackend.typeDcl_type_spec(self.rawtree))
	def get_dcls(self):
		return IDLList(pyIDLBackend.member_dcls(self.rawtree))

class IDLEnum(IDLIdent):
	"IDLEnum"
	def get_raw_ident(self):
		return pyIDLBackend.enum_ident(self.rawtree)
	def get_list(self):
		return IDLList(pyIDLBackend.enum_enumerator_list(self.rawtree))
	
class IDLSequence(IDLTree):
	"IDLSequence"
	def get_typespec(self):
		return tree_factory(pyIDLBackend.sequence_simple_type_spec(self.rawtree))
	def get_bound(self):
		foo = pyIDLBackend.sequence_positive_int_const(self.rawtree)
		if pyIDLBackend.is_null(foo) == 1:
			return 0
		else:
			return IDLInteger(foo).get_value()

class IDLTypeOctet(IDLTree):
	"IDLTypeOctet"
	pass

class IDLTypeAny(IDLTree):
       """ IDLTypeAny """	       
       def get_type(self):
	       """ Returns the enumerated value for the Any type """
	       return IDLN_TYPE_ANY


def tree_factory(rawtree):
	nodetype = pyIDLBackend.node_type(rawtree)

	if nodetype == IDLN_NONE:
		raise "Error - tree is a none!\n"
	elif nodetype == IDLN_LIST:
		return IDLList(rawtree)
	elif nodetype == IDLN_INTERFACE:
		return IDLInterface(rawtree)
	elif nodetype == IDLN_MODULE:
		return IDLModule(rawtree)
	elif nodetype == IDLN_EXCEPT_DCL:
		return IDLExceptDcl(rawtree)
	elif nodetype == IDLN_MEMBER:
		return IDLMember(rawtree)
	elif nodetype == IDLN_OP_DCL:
		return IDLOpDcl(rawtree)
	elif nodetype == IDLN_PARAM_DCL:
		return IDLParamDcl(rawtree)
	elif nodetype == IDLN_CONST_DCL:
		return IDLConstDcl(rawtree)
	elif nodetype == IDLN_TYPE_STRUCT:
		return IDLStruct(rawtree)
	elif nodetype == IDLN_TYPE_ENUM:
		return IDLEnum(rawtree)
	elif nodetype == IDLN_IDENT:
		return IDLIdent(rawtree)
	elif nodetype == IDLN_TYPE_STRING:
		return IDLTypeString(rawtree)
	elif nodetype == IDLN_TYPE_INTEGER:
		return IDLTypeInteger(rawtree)
	elif nodetype == IDLN_TYPE_FLOAT:
		return IDLTypeFloat(rawtree)
	elif nodetype == IDLN_TYPE_BOOLEAN:
		return IDLTypeBoolean(rawtree)
	elif nodetype == IDLN_INTEGER:
		return IDLInteger(rawtree)
	elif nodetype == IDLN_STRING:
		return IDLString(rawtree)
	elif nodetype == IDLN_BOOLEAN:
		return IDLBoolean(rawtree)
	elif nodetype == IDLN_TYPE_DCL:
		return IDLTypeDcl(rawtree)
	elif nodetype == IDLN_TYPE_OCTET:
		return IDLTypeOctet(rawtree)
	elif nodetype == IDLN_TYPE_SEQUENCE:
		return IDLSequence(rawtree)
	elif nodetype == IDLN_TYPE_ANY:
		return IDLTypeAny (rawtree)
	elif nodetype == IDLN_FORWARD_DCL:
		return IDLForwardDcl (rawtree)
	else:
		raise "Couldn't identify object: ",nodetype

		
IDLN_NONE = pyIDLBackend.IDLN_NONE
IDLN_ANY = pyIDLBackend.IDLN_ANY
IDLN_LIST = pyIDLBackend.IDLN_LIST
IDLN_GENTREE = pyIDLBackend.IDLN_GENTREE
IDLN_INTEGER = pyIDLBackend.IDLN_INTEGER
IDLN_STRING = pyIDLBackend.IDLN_STRING
IDLN_WIDE_STRING = pyIDLBackend.IDLN_WIDE_STRING
IDLN_CHAR = pyIDLBackend.IDLN_CHAR
IDLN_WIDE_CHAR = pyIDLBackend.IDLN_WIDE_CHAR
IDLN_FIXED = pyIDLBackend.IDLN_FIXED
IDLN_FLOAT = pyIDLBackend.IDLN_FLOAT
IDLN_BOOLEAN = pyIDLBackend.IDLN_BOOLEAN
IDLN_IDENT = pyIDLBackend.IDLN_IDENT
IDLN_TYPE_DCL = pyIDLBackend.IDLN_TYPE_DCL
IDLN_CONST_DCL = pyIDLBackend.IDLN_CONST_DCL
IDLN_EXCEPT_DCL = pyIDLBackend.IDLN_EXCEPT_DCL
IDLN_ATTR_DCL = pyIDLBackend.IDLN_ATTR_DCL
IDLN_OP_DCL = pyIDLBackend.IDLN_OP_DCL
IDLN_PARAM_DCL = pyIDLBackend.IDLN_PARAM_DCL
IDLN_FORWARD_DCL = pyIDLBackend.IDLN_FORWARD_DCL
IDLN_TYPE_INTEGER = pyIDLBackend.IDLN_TYPE_INTEGER
IDLN_TYPE_FLOAT = pyIDLBackend.IDLN_TYPE_FLOAT
IDLN_TYPE_FIXED = pyIDLBackend.IDLN_TYPE_FIXED
IDLN_TYPE_CHAR = pyIDLBackend.IDLN_TYPE_CHAR
IDLN_TYPE_WIDE_CHAR = pyIDLBackend.IDLN_TYPE_WIDE_CHAR
IDLN_TYPE_STRING = pyIDLBackend.IDLN_TYPE_STRING
IDLN_TYPE_WIDE_STRING = pyIDLBackend.IDLN_TYPE_WIDE_STRING
IDLN_TYPE_BOOLEAN = pyIDLBackend.IDLN_TYPE_BOOLEAN
IDLN_TYPE_OCTET = pyIDLBackend.IDLN_TYPE_OCTET
IDLN_TYPE_ANY = pyIDLBackend.IDLN_TYPE_ANY
IDLN_TYPE_OBJECT = pyIDLBackend.IDLN_TYPE_OBJECT
IDLN_TYPE_TYPECODE = pyIDLBackend.IDLN_TYPE_TYPECODE
IDLN_TYPE_ENUM = pyIDLBackend.IDLN_TYPE_ENUM
IDLN_TYPE_SEQUENCE = pyIDLBackend.IDLN_TYPE_SEQUENCE
IDLN_TYPE_ARRAY = pyIDLBackend.IDLN_TYPE_ARRAY
IDLN_TYPE_STRUCT = pyIDLBackend.IDLN_TYPE_STRUCT
IDLN_TYPE_UNION = pyIDLBackend.IDLN_TYPE_UNION
IDLN_MEMBER = pyIDLBackend.IDLN_MEMBER
IDLN_NATIVE = pyIDLBackend.IDLN_NATIVE
IDLN_CASE_STMT = pyIDLBackend.IDLN_CASE_STMT
IDLN_INTERFACE = pyIDLBackend.IDLN_INTERFACE
IDLN_MODULE = pyIDLBackend.IDLN_MODULE
IDLN_BINOP = pyIDLBackend.IDLN_BINOP
IDLN_UNARYOP = pyIDLBackend.IDLN_UNARYOP
IDLN_CODEFRAG = pyIDLBackend.IDLN_CODEFRAG
IDLN_VOID = IDLN_CODEFRAG + 99
