import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	elif tree.get_type() == pyIDL.IDLN_FORWARD_DCL:
		assert tree.get_name(1) == "bar"
		i = tree.get_referred_node()
		assert i.get_type() == pyIDL.IDLN_INTERFACE
	elif tree.get_type() == pyIDL.IDLN_INTERFACE: pass
	else:
		print "type = ",tree.get_type()
		assert 0, "got an unidentified type"
	

tree = pyIDL.parse_file(os.environ["srcdir"]+"/forward.idl")
process_node(tree)
