import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	elif tree.get_type() == pyIDL.IDLN_TYPE_ENUM:
		assert tree.get_name(1) == "TestEnum"
		list = tree.get_list()
		for item in list:
			if (item.get_name(1) != "ONE") & (item.get_name(1) != "TWO"):
				assert false
	elif tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	elif tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())
	
	# This is the real test - once we've got to the ident, we
	# assert that the referred node is the enum
	elif tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		ident = tree.get_typespec()
		enum = ident.get_referred_node()
		assert enum.get_type() == pyIDL.IDLN_TYPE_ENUM

tree = pyIDL.parse_file(os.environ["srcdir"]+"/enum.idl")
process_node(tree)
