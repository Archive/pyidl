import pyIDL
import os


#Recurse to find the target node,
#then assert that it is what we expect
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	elif tree.get_type() == pyIDL.IDLN_TYPE_SEQUENCE:
		assert tree.get_name(1) == "TestSequence"
		list = tree.get_list()
		for item in list:
			if (item.get_name(1) != "ONE") & (item.get_name(1) != "TWO"):
				assert false
	elif tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	elif tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())
	
	# This is the real test - once we've got to the ident, we
	# assert that the referred node is the sequence
	if tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		ident = tree.get_typespec()
		if(ident.get_name(1) == "TestSequence"):
			referred_node = ident.get_referred_node()
			assert referred_node.get_type() == pyIDL.IDLN_TYPE_DCL
			sequence = referred_node.get_typespec()
			assert sequence.get_type() == pyIDL.IDLN_TYPE_SEQUENCE
			assert sequence.get_typespec().get_type() == pyIDL.IDLN_TYPE_STRING
			assert sequence.get_bound() == 0
		else:
			assert ident.get_name(1) == "TestBoundedSequence"
			referred_node = ident.get_referred_node()
			assert referred_node.get_type() == pyIDL.IDLN_TYPE_DCL
			sequence = referred_node.get_typespec()
			assert sequence.get_type() == pyIDL.IDLN_TYPE_SEQUENCE
			assert sequence.get_typespec().get_type() == pyIDL.IDLN_TYPE_INTEGER
			assert sequence.get_bound() == 13

		
tree = pyIDL.parse_file(os.environ["srcdir"]+"/sequence.idl")
process_node(tree)
