#!/usr/bin/env python

import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	if tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	if tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())

	if tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		target =  tree.get_typespec()
		assert target.get_type()== pyIDL.IDLN_TYPE_BOOLEAN, "target is a boolean"

tree = pyIDL.parse_file(os.environ["srcdir"]+"/boolean.idl")
process_node(tree)
