import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	if tree.get_type() == pyIDL.IDLN_CONST_DCL:
		name = tree.get_name(0)

		if name == "CONST_SHORT":
			typespec = tree.get_typespec()
			assert typespec.get_type() == pyIDL.IDLN_TYPE_INTEGER, "const is an integer"
			assert typespec.get_width() == pyIDL.IDLTypeInteger.SHORT, "type is a short"
			assert tree.get_exp().get_value() == 0x1234, "short value is correct"
		elif name == "CONST_LONG":
			typespec = tree.get_typespec()
			assert typespec.get_type() == pyIDL.IDLN_TYPE_INTEGER, "const is an integer"
			assert typespec.get_width() == pyIDL.IDLTypeInteger.LONG, "type is a long"
			assert tree.get_exp().get_value() == 0x12345678, "long value is correct"

		elif name == "CONST_STRING":
			typespec = tree.get_typespec()
			assert typespec.get_type() == pyIDL.IDLN_TYPE_STRING, "const is a string"
			assert tree.get_exp().get_type() == pyIDL.IDLN_STRING, "const value is a string"
			assert tree.get_exp().get_value() == "CONST_STRING", "string value is correct"
		elif name == "CONST_BOOLEAN_TRUE":
			typespec = tree.get_typespec()
			assert typespec.get_type() == pyIDL.IDLN_TYPE_BOOLEAN, "const is a boolean"
			assert tree.get_exp().get_type() == pyIDL.IDLN_BOOLEAN, "const value is a boolean"
			assert tree.get_exp().get_value() == 1, "boolean value is true"
		elif name == "CONST_BOOLEAN_FALSE":
			typespec = tree.get_typespec()
			assert typespec.get_type() == pyIDL.IDLN_TYPE_BOOLEAN, "const is a boolean"
			assert tree.get_exp().get_type() == pyIDL.IDLN_BOOLEAN, "const value is a boolean"
			assert tree.get_exp().get_value() == 0, "boolean value is false"
		else:
			assert 0



tree = pyIDL.parse_file(os.environ["srcdir"]+"/consts.idl")
process_node(tree)
