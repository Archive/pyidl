import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	elif tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	elif tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())
	
	# This is the real test - once we've got to the ident, we
	# assert that the referred node is the enum
	elif tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		octet = tree.get_typespec()
		assert octet.get_type() == pyIDL.IDLN_TYPE_OCTET

tree = pyIDL.parse_file(os.environ["srcdir"]+"/octet.idl")
process_node(tree)
