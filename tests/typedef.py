import pyIDL
import os

"""
recurse to find the target node,
then assert that it is what we expect
"""
def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	elif tree.get_type() == pyIDL.IDLN_TYPE_DCL:
		typespec = tree.get_typespec()
		assert typespec.get_type() == pyIDL.IDLN_TYPE_STRING, "typedef is for a string"
		for ident in tree.get_dcls():
			assert ident.get_name(1) == "string_type", "typedef identifier is correct"
			
	elif tree.get_type() == pyIDL.IDLN_INTERFACE:
		process_node(tree.get_contents())
	if tree.get_type() == pyIDL.IDLN_OP_DCL:
		process_node(tree.get_arguments())
	
	# This is the real test - once we've got to the ident, we
	# assert that the referred node is the typedef
	if tree.get_type() == pyIDL.IDLN_PARAM_DCL:
		ident = tree.get_typespec()
		assert ident.get_name(1) == "string_type"
		referred_node = ident.get_referred_node()
		assert referred_node.get_type() == pyIDL.IDLN_TYPE_DCL
		assert referred_node.get_typespec().get_type() == pyIDL.IDLN_TYPE_STRING

tree = pyIDL.parse_file(os.environ["srcdir"]+"/typedef.idl")
process_node(tree)
