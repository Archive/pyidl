#!/usr/bin/env python

import pyIDL
import os

def process_node(tree):
	if tree.get_type() == pyIDL.IDLN_LIST:
		for node in tree:
			process_node(node)
	if tree.get_type() == pyIDL.IDLN_EXCEPT_DCL:
		process_node(tree.get_contents())

	if tree.get_type() == pyIDL.IDLN_MEMBER:
		assert(tree.get_typespec().get_type() == pyIDL.IDLN_TYPE_STRING)
		dcls = tree.get_dcls()
		assert dcls[0].get_name(1) == "why1"
		assert dcls[1].get_name(1) == "why2"

tree = pyIDL.parse_file(os.environ["srcdir"]+"/exception.idl")
process_node(tree)

